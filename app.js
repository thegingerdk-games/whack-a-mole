const squares = document.querySelectorAll('.square')
const mole = document.querySelectorAll('.mole')
const timeLeft = document.querySelector('#time-left')
const score = document.querySelector('#score')
const startButton = document.querySelector('#start')
const gameBoard = document.querySelector('#game-board')

let result = 0
let currentTime = +timeLeft.textContent
let hitPosition, timerId, moleTimer
let moleTimeout = 1000
let timeout = 1000

function randomSquare() {
    squares.forEach(square => {
        square.classList.remove('mole')
    })

    const randomPosition = squares[Math.floor(Math.random() * squares.length)]
    if(hitPosition !== randomPosition.id) {
        randomPosition.classList.add('mole')
        hitPosition = randomPosition.id
    } else {
        randomSquare()
    }
}

squares.forEach(square => {
    square.addEventListener('mouseup', (e) => {
        if(square.id === hitPosition) {
            result++
            score.textContent = result.toString()
            moveMole()
        }
    })
})

function moveMole (){
    clearInterval(moleTimer)
    randomSquare()
    moleTimer = setInterval(randomSquare, moleTimeout)
}

function countDown (){
    currentTime--
    timeLeft.textContent = currentTime.toString()

    if(currentTime === 30) {
        moleTimeout = 500
    }
    if(currentTime === 0) {
        clearInterval(timerId)
        clearInterval(moleTimer)
        alert('GAME OVER! Your final score is: ' + result)
        gameBoard.style.display = 'none'
        startButton.style.display = 'block'
    }
}

function start (){
    gameBoard.style.display = 'block'
    startButton.style.display = 'none'
    timeLeft.textContent = '60'
    result = 0
    moleTimeout = 1000
    timerId = setInterval(countDown, timeout)
    moveMole()
}

startButton.addEventListener('click', start)